import React from 'react'

const AddLocation = ( )=>{
    return (
        <div className="w-full rounded-2xl h-3/5 p-6 flex justify-center mt-28">
            <div>
            <button className="bg-button-color rounded-full h-12 w-full color-principal font-bold block">Add Location</button>
            <img src="/assets/images/best-place.svg" className="w-full h-full"></img>
            </div>
        </div>

    );
}

export default AddLocation;