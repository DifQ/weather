import React from "react";
import IconWeather from "./IconWeather";
const Clips = ({ weather, grados }) => {
  return (
    <>
      <div className="absolute ">
        <div className="absolute h-20 w-20 mb-2 bg-blue bg-color-secondary z-10 label-border-top shadow-2xl">
          <IconWeather
            stylesFirstIcon="relative h-8 w-8 mx-auto mt-4 z-10"
            stylesSecondIcon="absolute h-6 w-6 mx-auto mt-4 ml-3 top-0 left-0"
            weather={weather? weather.toLowerCase() : ""}
            colorCloud="cloud.svg"
          />

          <p className="font-principal text-home text-center text-white">
            {weather}
          </p>
          <div className="absolute h-6 w-4 triangle-border-top -top-2 left-0  bg-color-secondary"></div>
        </div>

        <div className="flex justify-center h-20 mt-14 w-20 bg-blue bg-color-principal label-border-bottom shadow-2xl">
          <p className="absolute text-3xl text-white tracking-tightert font-bold text-center mx-auto mt-8 font-principal">
            {grados}
            <p className="absolute inline text-white text-sm mt-1 font-principal">
              °c
            </p>
          </p>
          <div className="absolute h-2 w-2 triangle-border-bottom bg-color-principal -bottom-2 left-0 transform rotate-90 "></div>
        </div>
      </div>
    </>
  );
};

export default Clips;
