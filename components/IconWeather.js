import React from "react";

const IconWeather = ({ weather, stylesFirstIcon, stylesSecondIcon,colorCloud }) => {
  return (
    <>
      {weather === "clouds" ? (
        <>
          <img
            src={`/assets/images/${colorCloud}`}
            className={stylesFirstIcon}
          ></img>
          <img
            src={`/assets/images/${colorCloud}`}
            className={stylesSecondIcon}
          ></img>
        </>
      ) : weather === "rain" ? (
        <>
          <img
            src="/assets/images/cloud-rain.svg"
            className={stylesFirstIcon}
          ></img>
        
        </>
      ) : weather === "clear" ? (
        <img
          src="/assets/images/sun.svg"
          className={stylesFirstIcon}
        ></img>
      ) : (
        <img
          src={`/assets/images/${colorCloud}`}
          className={stylesFirstIcon}
        ></img>
      )}
    </>
  );
};
export default IconWeather;
