import React, { useEffect, useState } from "react";
import Label from "./Clips";
const Home = () => {
  const [weatherData, setWeatherData] = useState(null);
  useEffect(() => {
    var req = new XMLHttpRequest();
    req.responseType = "json";
    req.open(
      "GET",
      `https://api.openweathermap.org/data/2.5/onecall?lat=4.624335&lon=-74.063644&units=metric&exclude=hourly,minutely&appid=${process.env.WEATHER_API_KEY}`,
      true
    );
    req.onload = function () {
      var jsonResponse = JSON.parse(JSON.stringify(req.response));
      setWeatherData(jsonResponse);
    };
    req.send(null);
  }, []);

  return (
    <div className="relative flex items-center  w-full">
      <div
        style={{ height: "20vw" }}
        className="bg-image sm:h-screen rounded-3xl flex mx-12 mt-12 pl-20 pt-20 w-screen"
      >
        <h3 className="color-principal text-lg font-bold font-principal">
          <img
            alt="Home"
            className="color-principal w-4 h-4 inline m-1"
            src="/assets/images/location.svg"
          ></img>
          Bogotá
        </h3>
      </div>
      <Label
        weather={weatherData ? weatherData.current.weather[0].main : ""}
        grados={weatherData ? weatherData.current.temp : ""}
      />
    </div>
  );
};

export default Home;
