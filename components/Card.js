import React from "react";
import IconWeather from "./IconWeather";
const Card = ({ grados, color, image, day, weather }) => {
  return (
    <div className="grid grid-cols-4 sm:grid-cols-3 mt-4 mx-4 shadow-lg rounded-2xl animation-card">
      <div className="col-span-3 sm:col-span-2 flex items-center">
        <IconWeather
          stylesFirstIcon="relative w-8 h-8 ml-4"
          weather={weather ? weather.toLowerCase() : ""}
          stylesSecondIcon="absolute h-6 w-6 mx-auto mt-4 ml-3 top-0 left-0"
          colorCloud="cloud-principal.svg"
        />
        <div>
          <p className="font-principal text-xs font-bold text-gray-600 ml-2 block">
            {day}
          </p>
          <p className="font-principal text-xs font-bold text-gray-400 ml-2">
            {weather}
          </p>
        </div>
      </div>
      <div className={"w-full h-20 rounded-2xl p-2 ".concat(color)}>
        <p className="font-principal text-xs font-bold  text-center mt-6">
          {grados.split(" ")[0]}
          <p className="font-principal text-xs inline"> ° </p> /{" "}
          {grados.split(" ")[1]}
          <p className="font-principal text-xs inline"> ° </p>{" "}
        </p>
      </div>
    </div>
  );
};

export default Card;
