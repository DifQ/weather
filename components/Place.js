import React from "react";

const Place = ({text,styles,isWidthButton}) => {
  return (
    <div className={styles}>
      <p className="font-normal text-white text-sm font-bold w-2/3">
        <img
          className="color-principal w-4 h-4 inline m-1"
          src="/assets/images/location-white.svg"
        ></img>
        {text}
        
      </p>
      {isWidthButton?
     
      <button className="w-20 h-full bg-color-principal absolute inset-y-0 right-0 rounded-full text-2xl font-bold text-white">+</button>: null
    }
    </div>
  );
};

export default Place;
