import React from "react";
const CustomImage = ({ image, styles,alt }) => {
  return <img alt={alt} className={styles} src={`/assets/images/${image}`}></img>;
};

export default CustomImage;
