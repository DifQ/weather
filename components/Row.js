import React, { useEffect, useState, useCallback } from "react";
import AddLocation from "./AddLocation";
import Card from "./Card";
import CardLocation from "./CardLocation";
import CustomImage from "./CustomImage";
import Place from "./Place";
import { days } from "../utils/addDays";
const Row = () => {
  const [weatherDataBogota, setWeatherDataBogota] = useState(null);
  const [weatherDataParis, setWeatherDataParis] = useState(null);
  const [weatherDataLyon, setWeatherDataLyon] = useState(null);
  const [currentDate] = useState(new Date());
  const httpRequest = useCallback(
    (lat, long, city) => {
      var req = new XMLHttpRequest();
      req.responseType = "json";
      req.open(
        "GET",
        `https://api.openweathermap.org/data/2.5/onecall?lat=${lat}&lon=${long}&units=metric&exclude=hourly,minutely&appid=${process.env.WEATHER_API_KEY}`,
        true
      );
      req.onload = function () {
        var jsonResponse = JSON.parse(JSON.stringify(req.response));
        city === 1
          ? setWeatherDataBogota(jsonResponse)
          : city === 2
          ? setWeatherDataParis(jsonResponse)
          : city === 3
          ? setWeatherDataLyon(jsonResponse)
          : null;
      };
      req.send(null);
    },
    [setWeatherDataParis, setWeatherDataBogota, setWeatherDataLyon]
  );

  useEffect(() => {
    httpRequest("4.624335", "-74.063644", 1);
    httpRequest("48.8032", "-2.351148", 2);
    httpRequest("45.7484600", "4.8467100", 3);
  }, []);

  return (
    <div className="grid sm:grid-cols-2 xs:grid-cols-2 lg:grid-cols-4 gap-6 m-12">
      <div>
        <p className="font-extrabold text-xl font-principal">
          3 days
          <p className="inline font-normal ml-1 font-principal text-gray-600">
            Forecast
          </p>
        </p>
        {weatherDataBogota && weatherDataBogota.daily.length > 3
          ? weatherDataBogota.daily
              .slice(1, 4)
              .map((d, i) => (
                <Card
                  key={i}
                  grados={d.temp.night.toString().concat(" " + d.temp.day)}
                  day={days[currentDate.addDays(i + 1).getDay()]}
                  weather={d.weather[0].main}
                  color={
                    i == 0
                      ? "bg-color-principal text-white"
                      : "bg-color-helper-card "
                  }
                />
              ))
          : null}
      </div>
      <div>
        <p className="font-bold text-xl font-principal ml-6">
          Place to
          <p className="inline font-normal ml-1 font-principal text-gray-600">
            Visit
          </p>
        </p>
        <div className="p-6 w-full h-full">
          <Place
            styles="bg-travel-singapur w-full h-full rounded-3xl p-2 animation-image"
            text="Marina Bay, Singapore, Singapur"
          />
        </div>
      </div>
      <div>
        <div className="flex justify-end">
          <p className="font-principal text-gray-500 font-bold text-sm">
            +
            <p className="inline font-principal text-gray-400 ml-2 text-sm">
              Top raitings
            </p>
          </p>
          <CustomImage
            styles="w-6 h-6 rounded-md mr-1 ml-3"
            image="person1.jpg"
          />
          <CustomImage styles="w-6 h-6 rounded-md ml-1" image="person2.jpg" />
          <CustomImage styles="w-6 h-6 rounded-md ml-1" image="person3.jpg" />
          <div className="w-6 h-6 rounded-md bg-color-helper ml-1 mr-6 text-white text-center pt-1 text-xs font-principal font-bold">
            6+
          </div>
        </div>
        <div className="p-6 h-32">
          <Place
            styles="bg-travel-paris w-full rounded-3xl p-2 h-32 animation-image"
            text="Paris, France"
          />
        </div>
        <div className="p-6 h-3/5">
          <Place
            styles="bg-travel-new-york w-full rounded-3xl p-2 mt-1 animation-image relative bg-rounded "
            text="Times Square, New York, United States" isWidthButton={true}
          />
        </div>
      </div>
      <div>
        {weatherDataLyon ? (
          <CardLocation
            grados={weatherDataLyon.current.temp}
            weather={weatherDataLyon.current.weather[0].main}
            pais="Francia"
            ciudad="Lyon"
            ambiente={weatherDataLyon.current.humidity}
            posicion="west"
            velocidadViento={weatherDataLyon.current.wind_speed}
          />
        ) : null}

        <div className="lg:absolute">
          {weatherDataParis ? (
            <CardLocation
              grados={weatherDataParis.current.temp}
              weather={weatherDataParis.current.weather[0].main}
              pais="Francia"
              ciudad="Paris"
              ambiente={weatherDataParis.current.humidity}
              posicion="NorthWest"
              velocidadViento={weatherDataParis.current.wind_speed}
              styles="lg:bottom-4 lg:left-0"
            />
          ) : null}
        </div>
        <AddLocation/>
      </div>
    </div>
  );
};

export default Row;
