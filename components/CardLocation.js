import React from "react";
import IconWeather from "./IconWeather";

const CardLocation = ({ grados,weather, pais,ciudad,ambiente,posicion,velocidadViento,styles }) => {
  return (
    <div className={"lg:absolute mt-12 lg:mt-0 w-full rounded-3xl bg-white shadow-2xl lg:w-64 h-28 animation-image ".concat(styles)}>
      <div className="grid grid-cols-4 flex items-center">
        <div className=" w-full h-20 bg-color-helper-card rounded-t-3xl rounded-2xl">
          <IconWeather stylesFirstIcon="relative w-8 h-8 ml-4 mt-6" weather={weather ? weather.toLowerCase():""} colorCloud="cloud-principal.svg" stylesSecondIcon="absolute h-6 w-6 mx-auto mt-4 ml-3 top-0 left-0"></IconWeather>
        </div>
        <div>
          <p className="w-full font-principal font-bold text-center mx-auto ml-2  text-2xl font-extrabold">
            {grados} <p className="inline font-principal text-xs mb-12">°c</p>
          </p>
        </div>
        <div>
             <div>
          <p className="font-principal text-xs font-extrabold ml-3 block">
            {pais}
          </p>
          <p className="font-principal text-xs font-bold text-gray-600 ml-3">
            {ciudad}
          </p>
        </div>
        </div>
      </div>
      <div className="flex justify-between mx-4 my-2">
        <p className="font-principal text-gray-400 text-xs ">Humidity {ambiente}%</p> 
        <p className="font-principal text-gray-400 text-xs ">{posicion}</p> 
        <p className="font-principal text-gray-400 text-xs ">{velocidadViento}km/h</p> 
      </div>
    </div>
  );
};

export default CardLocation;
