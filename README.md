# Weather David Proyecto

Proyecto que permite visualizar el clima de diferentes ciudades en tiempo real 

# Tecnologias utilizadas

  ### Front end
  - React js
  - Next js
  - tailwindcss
  ### Deployment
  - Vercel


### Instalación del proyecto

El proyecto require [Node.js](https://nodejs.org/) para correr

```sh
$ npm install
$ npm run dev
```
