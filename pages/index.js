import Home from '../components/Home'
import Row from '../components/Row'
import Head from 'next/head'

export default function () {
  return (
    <div >
      <Head>
        <meta name="google-site-verification" content="I2rmszfVgD7_PA-vXXV4szJSYGnocuUoi3ktzGUZdEU" />
      </Head>
      <Home></Home>
      <Row></Row>
    </div>
  )
}
